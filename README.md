# symfony/security

Security system for web applications. https://symfony.com/components/Security

# Security Component is made of:
* security-core
* security-csrf
* security-guard (deprecated)
* security-http

Look on the separate projects for specific information.

Here is only information concerning integrating of a few of the sub-components!

# Official documentation
* [*Security*
  ](https://symfony.com/doc/current/security.html)
* ~[*Using the new Authenticator-based Security*
  ](https://symfony.com/doc/current/security/authenticator_manager.html) (Core, Http)~

## New features, evolution, and deprecation
* [*New in Symfony 5.3: Guard Component Deprecation*
  ](https://symfony.com/blog/new-in-symfony-5-3-guard-component-deprecation)
  2021-06 Javier Eguiluz
* [*New in Symfony 4.4: Lazy Firewalls*
  ](https://symfony.com/blog/new-in-symfony-4-4-lazy-firewalls)
  2019-10 Javier Eguiluz

# Unofficial documentation
* [*Symfony’s Security Key Concepts in 5 minutes:
Let’s go over the obscure terms and concepts of this critical component*
  ](https://alex-daubois.medium.com/symfonys-security-key-concepts-in-5-minutes-e18033c26b35)
  2021-12 Alexandre Daubois
* (fr) [*Limiter le nombre de tentatives de connexions sous Symfony*
  ](https://blog.netinfluence.ch/2019/04/18/limiter-le-nombre-de-tentatives-de-connexions-sous-symfony/)
  2019-04 [Romaric](https://blog.netinfluence.ch/author/romaric/)
  * security-core
  * security-csrf
  * security-guard
  * security-http

# Java Spring framework inspiration in Symfony
* [Spring Security](https://spring.io/projects/spring-security)
  * [*Understanding Symfony Security by Using it Standalone*](https://wouterj.nl/2019/03/understanding-symfony-security-by-using-it-standalone)
    2019-03 Wouter J
* https://spring.io/projects/spring-framework
* [symfony Java Spring framework](https://google.com/search?q=symfony+Java+Spring+framework)

## Official recognition
* https://github.com/symfony/security-core
* https://github.com/symfony/security-http
